

# TAG PUZZLE GAME ON RUST

Using raylib and rand()/srand() from libc


Clone:


```git clone https://gitlab.com/vada4367/tag_game_rust_raylib```


Build:

```cargo build --release```


Run:

```cargo run --release```


FMT:

```cargo fmt```
