use raylib::prelude::*;
use std::fmt::{Display, Formatter};

#[link(name = "c")]
extern "C" {
    fn rand() -> usize;
    fn time(time: *mut usize) -> usize;
    fn srand(seed: usize);
}

struct Puzzle {
    cell_size: usize,
    w: usize,
    h: usize,
    array: Vec<Vec<usize>>,
}

impl Puzzle {
    fn new(_cell_size: usize, _w: usize, _h: usize) -> Self {
        Self {
            cell_size: _cell_size,
            w: _w,
            h: _h,
            array: vec![vec![0; _w]; _h],
        }
    }

    fn rand(&mut self) {
        let (mut x, mut y) = (0, 0);
        for i in 1..self.w * self.h {
            while self.array[y][x] != 0 {
                x = unsafe { rand() } % self.w;
                y = unsafe { rand() } % self.h;
            }

            self.array[y][x] = i;
        }
    }

    fn play(&mut self, x: usize, y: usize) -> Result<(), ()> {
        if x >= self.w || y >= self.h {
            eprintln!("X > self.w || y > self.h");
            return Err(());
        }

        if x != 0 && self.array[y][x - 1] == 0 {
            self.array[y][x - 1] = self.array[y][x];
            self.array[y][x] = 0;
            return Ok(());
        }
        if x != self.w - 1 && self.array[y][x + 1] == 0 {
            self.array[y][x + 1] = self.array[y][x];
            self.array[y][x] = 0;
            return Ok(());
        }
        if y != 0 && self.array[y - 1][x] == 0 {
            self.array[y - 1][x] = self.array[y][x];
            self.array[y][x] = 0;
            return Ok(());
        }
        if y != self.h - 1 && self.array[y + 1][x] == 0 {
            self.array[y + 1][x] = self.array[y][x];
            self.array[y][x] = 0;
            return Ok(());
        }

        return Err(());
    }

    fn draw(
        &self,
        rl: &mut RaylibHandle,
        thread: &RaylibThread,
    ) {
        let (window_w, window_h) = (rl.get_screen_width() as usize, rl.get_screen_height() as usize);
        let mut d = rl.begin_drawing(&thread);

        d.clear_background(Color::BLACK);

        for i in 0..self.h {
            for j in 0..self.w {
                d.draw_text(
                    &format!("{}", self.array[i][j]),
                    (window_h / self.h * j)
                        as i32,
                    (window_w / self.w * i)
                        as i32,
                    30,
                    Color::WHITE,
                );
            }
        }
    }
}

impl Display for Puzzle {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut result_string = String::new();

        for i in 0..self.h {
            for j in 0..self.w {
                match self.array[i][j] {
                    0 => {
                        result_string.push_str(&format!("{:2}", ' '))
                    }
                    num => {
                        result_string.push_str(&format!("{:2}", num))
                    }
                }
            }

            result_string.push('\n');
        }

        write!(f, "{}", result_string)
    }
}

fn main() {
    unsafe {
        srand(time(std::ptr::null_mut()));
    }

    let mut puzzle = Puzzle::new(70, 6, 5);
    puzzle.rand();

    let (mut rl, thread) =
        raylib::init().size((puzzle.cell_size * puzzle.w) as i32, (puzzle.cell_size * puzzle.h) as i32).title("TAG_GAME").build();

    while !rl.window_should_close() {
        puzzle.draw(&mut rl, &thread);
        

        if !rl.is_mouse_button_up(MouseButton::MOUSE_LEFT_BUTTON) {
            let (x, y) = (rl.get_mouse_x() as usize / puzzle.cell_size, rl.get_mouse_y() as usize / puzzle.cell_size);
            let _ = puzzle.play(x, y);
        }
    }
}
